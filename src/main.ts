import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Database } from 'arangojs';
import { createServer } from 'node:http';
import * as fs from 'fs';
import { createSchema, createYoga } from 'graphql-yoga';
import { resolvers } from './resolvers/index';
const dotenv = require('dotenv');
dotenv.config();



const db = new Database({
  url: process.env.ARANGODB_URL,
  databaseName: process.env.ARANGODB_DB,
});
db.useBasicAuth(process.env.ARANGODB_USERNAME,process.env.ARANGODB_PASSWORD);

const typeDefs = fs.readFileSync('./src/schema.gql', 'utf8');
const yoga = createYoga({
  context: { },
  schema: createSchema({
    typeDefs,
    resolvers,
  }),
});


const server = createServer(yoga);

server.listen(3000, () => {
  console.info('Server is running on http://localhost:3000/graphql');
});

export { db }

