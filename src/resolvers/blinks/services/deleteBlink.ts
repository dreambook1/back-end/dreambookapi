import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { getUser } from "../../users";

const deleteBlink = async (parent, args, context, info): Promise<String> => {
  const { _id } = args;

  const user = await getUser(parent, { _id: context.user._id }, context, info);
  if (!user.dreams.some((dream) => dream.blinks.some((blink) => blink._id === _id))) {
    throw new ForbiddenError("Dream doesn't belong to user");
  }
  const edgeBlinkIDReq = await db.query(aql`
    LET blinkId = ${_id}
    FOR edge IN dreams_blinks
        FILTER edge._to == blinkId
        RETURN edge._key
  `);
  const edgeBlinkID: any[] = await edgeBlinkIDReq.next();

  const _key = _id.replace(/\D/g,'')
  const blinkDeleteReq = await db.query(aql`
    REMOVE ${_key} IN Blinks
  `);
  const edgeBlinkDeleteReq = await db.query(aql`
    REMOVE ${edgeBlinkID} IN dreams_blinks
  `);

  return "Succefull";
};

export { deleteBlink };
