import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { getUser } from "../../users";

const createBlink = async (parent, args, context, info) => {
  const {description, dream_id } = args;

  const user = await getUser(parent, { _id: context.user._id }, context, info);

  if (!user.dreams.find(dream => dream._id === dream_id)) throw new ForbiddenError("You are not allowed to create a blink for this dream.");

  const BlinkReq = await db.query(aql`
    INSERT {
      description: ${description ?? null},
      date: DATE_NOW(),
      created_at: DATE_NOW(),
      updated_at: DATE_NOW(),
    } INTO Blinks
    RETURN NEW
  `);
  const Blink = await BlinkReq.next();

  await db.query(aql`
    FOR dream IN Dreams
      FILTER dream._id == ${dream_id}
      FOR blink IN Blinks
        FILTER blink._id == ${Blink._id}
        INSERT {
          _from: dream._id,
          _to: blink._id,
          active: true,
        } INTO dreams_blinks
  `);

  return Blink;
};

export { createBlink };
