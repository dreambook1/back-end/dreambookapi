import { createBlink } from "./services/createBlink";
import { deleteBlink } from "./services/deleteBlink";

export { createBlink, deleteBlink};
