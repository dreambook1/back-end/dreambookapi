import { createDream } from "./services/createDream";
import { getDream } from "./services/getDream";
import { getDreams } from "./services/getDreams";
import { deleteDream } from "./services/deleteDream";
import { addTag } from "./services/addTag";
import { removeTag } from "./services/removeTag";

export { createDream, getDream, getDreams, deleteDream, addTag, removeTag};
