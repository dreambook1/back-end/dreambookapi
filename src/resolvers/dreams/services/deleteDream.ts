import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { IDream } from "src/types/user/dream.type";
import { getUser } from "../../users";

const deleteDream = async (parent, args, context, info): Promise<String> => {
  const { _id } = args;

  const user = await getUser(parent, { _id: context.user._id }, context, info);
  if (!user.dreams.some((dream) => dream._id === _id)) throw new ForbiddenError("Dream doesn't belong to user");


  const blinksIDReq = await db.query(aql`
    LET dreamId = ${_id}
    FOR edge IN dreams_blinks
        FILTER edge._from == dreamId
        FOR blink IN Blinks
        FILTER blink._id == edge._to
        RETURN blink._key
  `);
  const blinksID: any[] = await blinksIDReq.all();

  const edgeBlinksIDReq = await db.query(aql`
    LET dreamId = ${_id}
    FOR edge IN dreams_blinks
        FILTER edge._from == dreamId
        RETURN edge._key
  `);
  const edgeBlinksID: any[] = await edgeBlinksIDReq.all();

  const edgeDreamsIDReq = await db.query(aql`
    LET dreamId = ${_id}
    FOR edge IN users_dreams
        FILTER edge._to == dreamId
        RETURN edge._key
  `);
  const edgeDreamsID: any = await edgeDreamsIDReq.next();

  const blinkDeleteReq = await db.query(aql`
    LET ids  = ${blinksID} 
    FOR id IN ids
        REMOVE id IN Blinks
`);

  const edgeDeleteReq = await db.query(aql`
    LET ids  = ${edgeBlinksID} 
    FOR id IN ids
        REMOVE id IN dreams_blinks
`);

  const edgedreamDeleteReq = await db.query(aql`
    REMOVE ${edgeDreamsID} IN users_dreams
`);

  const _key = _id.replace(/\D/g,'')
  const dreamDeleteReq = await db.query(aql`
    REMOVE ${_key} IN Dreams
`);

  return "Succefull";
};

export { deleteDream };
