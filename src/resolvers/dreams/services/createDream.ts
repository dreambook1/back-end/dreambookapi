import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { IDream } from "src/types/user/dream.type";
import { getUser } from "../../users";

const createDream = async (parent, args, context, info): Promise<IDream> => {
  const { name, tags} = args;
  const user = await getUser(parent, { _id: context.user._id }, context, info);

  let sanitizedTags;
  if (tags !== undefined){
    sanitizedTags = tags.map((tag) => tag[0].toUpperCase() + tag.substring(1).toLowerCase());
  }
  const dreamReq = await db.query(aql`
    INSERT {
      name: ${name ?? null},
      tags: ${sanitizedTags ?? [] as string[]},
      date: DATE_NOW(),
      created_at: DATE_NOW(),
      updated_at: DATE_NOW(),
    } INTO Dreams
    RETURN NEW
  `);
  const dream = await dreamReq.next();

  const edgeReq = await db.query(aql`
    FOR u IN Users
      FILTER u._id == ${user._id}
      FOR d IN Dreams
        FILTER d._id == ${dream._id}
        INSERT {
          _from: u._id,
          _to: d._id,
          active: true
        } INTO users_dreams
      RETURN NEW
  `);
  await edgeReq.next();

  return dream;
}

export { createDream };
