import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { IDream } from "src/types/user/dream.type";
import { getUser } from "../../users";
import { getDream } from "./getDream";

const patchDream = async (parent, args, context, info): Promise<IDream> => {
  const { _id, name } = args;

  const user = await getUser(parent, { _id: context.user._id }, context, info);
  if (!user.dreams.some((dream) => dream._id === _id)) throw new ForbiddenError("Dream doesn't belong to user");

  const actualDreamReq = await db.query(aql`
    FOR d IN Dreams
      FILTER d._id == ${_id}
      RETURN d
  `);
  const actualDream: IDream = await actualDreamReq.next();

  const dreamReq = await db.query(aql`
    FOR d IN Dreams
      FILTER d._id == ${_id}
      UPDATE d WITH {
        name: ${name ?? actualDream.name},
        updated_at: DATE_NOW(),
      } IN Dreams
      RETURN NEW
  `);
  const dream = await dreamReq.next();

  return await getDream(parent, { _id: dream._id }, context, info);
};

export { patchDream };
