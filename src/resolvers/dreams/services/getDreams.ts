import { aql } from "arangojs";
import { db } from "src/main";

const getDreams = async (parent, args, context, info) => {

  const DreamReq = await db.query(aql`
    FOR d IN Dreams
      RETURN d
  `);
  const Dreams = await DreamReq.all();

  return Dreams;
};

export { getDreams };
