import { aql } from "arangojs";
import { db } from "src/main";
import { IDream } from "src/types/user/dream.type";

const getDream = async (parent, args, context, info): Promise<IDream> => {
  const { _id } = args;

  const DreamReq = await db.query(aql`
    FOR d IN Dreams
      FILTER d._id == ${_id}
      RETURN d
  `);
  const Dream = await DreamReq.next();

  return Dream;
};

export { getDream };
