import { createUser } from "./services/createUser";
//import { deleteUser } from "./services/deleteUser";
import { getUser } from "./services/getUser";
import { getUsers } from "./services/getUsers";
//import { patchUser } from "./services/patchUser";

export { createUser, getUser, getUsers };
