import { ValidationError } from "@nestjs/apollo";
import * as PasswordValidator from "password-validator";
import * as sha1 from 'sha1';
import { IUser } from "src/types/user/user.type";

import { aql } from "arangojs";
import { db } from "src/main";

const userCreateManipulate = (u: IUser): IUser => {
  if (u.email) u.email = u.email.toLowerCase();
  if (u.password) u.password = sha1(u.password);
  return u;
};

const userCreateCheck = async (u: IUser): Promise<IUser> => {

  const existingUserReq = await db.query(aql`
    FOR u IN Users
      FILTER u.email == ${u.email}
      RETURN u
    `);

  if (existingUserReq.hasNext) throw new ValidationError("Email is already used");

  if (u.email.length < 1) throw new ValidationError("Email is required");
  if (!u.email.match(/^[A-Za-z0-9+_.-]+@(.+)$/)) throw new ValidationError("Email is invalid");

  let schema = new PasswordValidator();
  schema.is().min(4).has().uppercase().has().lowercase().has().digits();
  if (!schema.validate(u.password)) throw new ValidationError("Password is not strong enough");

  return userCreateManipulate(u);
}

const userPatchManipulate = (u: IUser): IUser => {
  if (u.email) u.email = u.email.toLowerCase();
  if (u.password) u.password = sha1(u.password);
  return u;
}

const userPatchCheck = (u?: IUser): IUser => {
  if (u.email && u.email.length < 1) throw new ValidationError("Email est obligatoire");
  if (u.email && !u.email.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) throw new ValidationError("Email n'est pas valide");

  if (u.password) {
    let schema = new PasswordValidator();
    schema.is().min(8).is().max(100).has().uppercase().has().lowercase().has().digits().has().not().spaces();
    if (!schema.validate(u.password)) throw new ValidationError("Le mot de passe n'est pas assez fort");
  }

  return userPatchManipulate(u);
}

export { userCreateCheck, userPatchCheck };
