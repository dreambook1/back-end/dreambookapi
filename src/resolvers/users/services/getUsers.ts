import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { hide } from "src/middleware/hideProperties";
import { IUser } from "src/types/user/user.type";

const getUsers = async (parent, args, context, info): Promise<IUser[]> => {
    const userReq = await db.query(aql`
    For u IN Users
        RETURN u
    `);
  
    const user: IUser[] = await userReq.all();
  
    return user;
  };
  
  export { getUsers };
  