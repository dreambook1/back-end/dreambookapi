import { ForbiddenError } from '@nestjs/apollo';
import { aql } from 'arangojs';
import { db } from 'src/main';
import { createToken } from 'src/middleware/auth';
import { IUser } from 'src/types/user/user.type';
import { ISleepTime } from 'src/types/user/sleepTime';
import { userCreateCheck } from '../check';

export const createUser = async (parent, args, context, info): Promise<IUser> => {
  const { email, password, newsletter, fcm_id } = await userCreateCheck(args);

  const userReq = await db.query(aql`
    INSERT {
      email: ${email},
      password: ${password},
      newsletter: TO_BOOL(${newsletter ?? false}),
      fcm_id: ${fcm_id ?? null},
      active: true,
      reset_token: null,
      premium: false,
      created_at: DATE_NOW(),
      updated_at: DATE_NOW(),
    } 
    INTO Users
    RETURN NEW
  `);

  const sleepTimeReq = await db.query(aql`
    INSERT {
      active: false,
      monday: "00:00",
      tuesday: "00:00",
      wednesday: "00:00",
      thursday: "00:00",
      friday: "00:00",
      saturday: "00:00",
      sunday: "00:00",
    } 
    INTO SleepTimes
    RETURN NEW
  `);

  let user: IUser = await userReq.next();
  user.sleepTime = await sleepTimeReq.next();

  console.log(user)
  const EdgeSleepTimeReq = await db.query(aql`
    INSERT {
      _from: ${user._id},
      _to: ${user.sleepTime._id},
    } 
    INTO users_sleeptimes
    RETURN NEW
  `);


  return {
    ...user,
    jwt: createToken(user),
  };
};
