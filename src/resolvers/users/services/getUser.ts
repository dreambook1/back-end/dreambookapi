import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { hide } from "src/middleware/hideProperties";
import { IUser } from "src/types/user/user.type";
import { arrayToString, parseStringToArray } from 'src/utils/arrayWrapper';

const getUser = async (parent, args, context, info): Promise<IUser> => {
  const { _id } = args;

  const userReq = await db.query(aql`

  LET user = DOCUMENT(Users, ${_id})

  LET dreams = (
      FOR dream IN ANY user users_dreams
          LET blinks = (
              FOR blink IN ANY dream dreams_blinks
                  RETURN blink
          )
      
      RETURN MERGE(dream, {blinks: blinks})
  )
  LET sleepTime= (
      FOR sleepTime IN ANY user users_sleeptimes
          RETURN sleepTime
  )

RETURN MERGE(user, { dreams: dreams }, { sleepTime: sleepTime[0] })

`);

  const user = await userReq.next();

  //let sanitizedUser = user 
  //sanitizedUser = user.dreams.forEach((dream) => { dream.tags = parseStringToArray(dream.tags) });

  return context.self ? user : hide(user, "email", "password");
};

export { getUser };
