import { createToken } from 'src/middleware/auth';
import * as sha1 from 'sha1';
import { db } from 'src/main';
import { aql } from 'arangojs';
import { IUser } from 'src/types/user/user.type';
import { AuthenticationError } from '@nestjs/apollo';

const authenticate = async (parent, args, context): Promise<IUser> => {
  const { email, password } = args;

  const userReq = await db.query(aql`
    FOR u IN Users
      FILTER u.email == ${email}
      RETURN u
  `);

  const user: IUser = await userReq.next();
  if (user?.password !== sha1(password))
    throw new AuthenticationError('Invalid credentials');

  const jwt = createToken(user);

  return {
    ...user,
    jwt: jwt,
  };
};

export { authenticate };

