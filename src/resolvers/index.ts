import { verifyToken } from "src/middleware/auth";
import { createDream, getDream, getDreams, deleteDream, addTag, removeTag} from "./dreams"
import { createUser, getUser, getUsers } from "./users";
import { authenticate } from "./authenticate";
import { createBlink, deleteBlink } from "./blinks";
import { patchSleepTime } from "./sleepTime"

interface IResolverArgs {
    parent: any;
    args: any;
    context: any;
    info: any;
  }

  
// Protect resolvers with jwt (mandatory) and roles (optional)
const p = (rf: Function) => async (...rA: IResolverArgs[]) => {
    rA[2] = await verifyToken(rA[2]);
    return rf(...rA);
  }

  
const resolvers = {
    Query: {
        authenticate,
        getUser: p(getUser),
        getUsers: p(getUsers),
        getDream: p(getDream),
        getDreams: p(getDreams),
        ping: () => 'Hello world!',
    },
    Mutation: {
        createUser,
        createDream: p(createDream),
        addTag: p(addTag),
        removeTag: p(removeTag),
        deleteDream: p(deleteDream),
        createBlink: p(createBlink),
        deleteBlink: p(deleteBlink),
        patchSleepTime: p(patchSleepTime),
    },
}

export { resolvers };