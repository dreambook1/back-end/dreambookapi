import { ForbiddenError } from "@nestjs/apollo";
import { aql } from "arangojs";
import { db } from "src/main";
import { getUser } from "../../users";
import { IUser } from "src/types/user/user.type";
import {sleepTimeDayFormatCheck} from "../check"

const patchSleepTime = async (parent, args, context, info): Promise<IUser> => {
  const { active, monday, tuesday, wednesday, thursday, friday, saturday, sunday } = await sleepTimeDayFormatCheck(args);

  const user = await getUser(parent, { _id: context.user._id }, context, info);

  const SleepTimeReq = await db.query(aql`
  LET usrid = ${user._id}

  LET sleeptimeID = (
  For x in users_sleeptimes
      FILTER x._from == usrid
      RETURN x._to
  )
  For s in SleepTimes
      FILTER s._id == sleeptimeID[0]
      UPDATE s WITH {
        active: ${active ?? user.sleepTime.active},
        monday: ${monday ?? user.sleepTime.monday},
        tuesday: ${tuesday ?? user.sleepTime.tuesday},
        wednesday: ${wednesday ?? user.sleepTime.wednesday},
        thursday: ${thursday ?? user.sleepTime.thursday},
        friday: ${friday ?? user.sleepTime.friday},
        saturday: ${saturday ?? user.sleepTime.saturday},
        sunday: ${sunday ?? user.sleepTime.sunday},
        updated_at: DATE_NOW(),
      } IN SleepTimes
      RETURN NEW
  `);

  return await getUser(parent, { _id: context.user._id }, context, info);
};

export { patchSleepTime };
