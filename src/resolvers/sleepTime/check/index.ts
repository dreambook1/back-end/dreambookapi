import { ValidationError } from "@nestjs/apollo";
import { ISleepTime } from "src/types/user/sleepTime";

const sleepTimeDayFormatCheck = async (s: ISleepTime): Promise<ISleepTime> => {
    const timeFormatRegex = /^(?:[01]\d|2[0-3]):[0-5]\d$/;
    const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

    days.forEach(day => {
        if (s[day] !== undefined) {
          const timeString = s[day];
          if (!timeFormatRegex.test(timeString)) {
            throw new ValidationError(`Bad format for ${day}`);
          }
        }
      });

    return s;
}

export{sleepTimeDayFormatCheck};