export function parseStringToArray(str: string): Array<string[]> | null {
    // Assuming the input string is formatted as a sequence of arrays without outer brackets
    const jsonStr = `[${str}]`;
  
    try {
      // Parse the JSON string back to an array of arrays
      const arr: Array<string[]> = JSON.parse(jsonStr);
      return arr;
    } catch (error) {
      console.error("Error parsing string to array:", error);
      return null; // or handle the error as needed
    }
}

export function arrayToString(arr: Array<string[]>): string {
    // Convert the array of arrays into a JSON string
    const jsonString = JSON.stringify(arr);
  
    // Remove the outer brackets [] to match the desired format
    // and handle any other formatting adjustments here if needed
    const formattedString = jsonString.slice(1, -1);
  
    return formattedString;
}