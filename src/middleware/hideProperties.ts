const boolFields = [
	"newsletter",
	"producer_intent"
];

const numberFields = [

];

const hide = (obj: any, ...props: string[]) => {
	props.forEach((prop) => {
		if (obj?.[prop]) {
			switch (true) {
				case boolFields.includes(prop):
					obj[prop] = false;
					break;
				case numberFields.includes(prop):
					obj[prop] = 0;
					break;
				default:
					obj[prop] = "";
					break;
			}
		}
	});

	return obj;
};

export { hide };
