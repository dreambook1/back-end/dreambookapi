import { AuthenticationError } from '@nestjs/apollo';
import * as fs from 'fs';
import * as jwt from 'jsonwebtoken';
import {IUser } from 'src/types/user/user.type';

const privateKEY = fs.readFileSync('./src/config/private.key', 'utf8');
const publicKEY = fs.readFileSync('./src/config/public.key', 'utf8');

const checkJwt = (token: string, context: any = {}): IUser => {
  try {
    const decoded: any = jwt.verify(token, publicKEY, { algorithms: ['RS256'] });

    if (decoded.exp < Date.now() / 1000) throw new AuthenticationError('Token Expired');

    return decoded;
  } catch (e) {
    throw new AuthenticationError('Invalid Token');
  }
};

const verifyToken = async (context): Promise<any> => {
  const authHeader = context.req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) throw new AuthenticationError('A token is required for authentication');

  let user = checkJwt(token);

  context.user = user;

  return context;
};

const createToken = (user: IUser): string => {
  const payload: any = {
    _id: user._id,
    email: user.email,
  };

  return jwt.sign(payload, privateKEY, {
    expiresIn: '183d',
    algorithm: 'RS256',
  });
};

export { checkJwt, verifyToken, createToken };

