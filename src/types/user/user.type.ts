import { IDream } from "./dream.type"
import { ISleepTime } from './sleepTime';

export interface IUser {
    _id: string
    email: string
    password: string
    newsletter?: boolean
    jwt?: string
    active: boolean
    verified_at?: string
    created_at: string
    updated_at: string
    fcm_id?: string
    dreams: IDream[]
    sleepTime?: ISleepTime
  }
