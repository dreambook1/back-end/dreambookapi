import { IBlinks } from "./blinks.type"

export interface IDream {
    _id: string
    date: string
    tags: string[]
    name: string
    blinks: [IBlinks]
  }
