export interface ISleepTime {
    _id: string
    active: boolean
    monday: string
    tuesday: string
    wednesday: string
    thursday: string
    friday: string
    saturday: string
    sunday: string
  }
  