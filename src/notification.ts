import * as amqp from 'amqplib';

export enum template{
    test,
    test2
}

export enum notifType{
    device,
    mail
}


async function sendNotification(user_idp: string, templatep: template, notifTypep: notifType) {

    try {
        const connection = await amqp.connect(process.env.RABBIT_MQ); 
        const channel = await connection.createChannel(); 
    
        const queue = 'dreambook';
        await channel.assertQueue(queue, {
          durable: false, 
        });
    
        channel.sendToQueue(queue, Buffer.from(JSON.stringify({user_id: user_idp, template: template[templatep], notifType: notifType[notifTypep]})));
    
        setTimeout(() => {
          connection.close();
        }, 500); 
      } catch (error) {
        console.error("Failed to send notification:", error);
      }
}

export { sendNotification };